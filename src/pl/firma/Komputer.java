package pl.firma;

public class Komputer {
	
	private int taktowanie;
	private int ram;
	
	public Komputer(int taktowanie, int ram) {
		this.taktowanie = taktowanie;
		this.ram = ram;
	}
	
	public int getTaktowanie() {
		return taktowanie;
	}
	public void setTaktowanie(int taktowanie) {
		this.taktowanie = taktowanie;
	}
	public int getRam() {
		return ram;
	}
	public void setRam(int ram) {
		this.ram = ram;
	}
}
