package pl.firma;

public class OuterClass {
	
	public static class InnerClass {
		public int property;
		//...
	}
	
//	public void someMethod() {
//	    final String finalVariable = "final variable";
//	    String effectivelyFinalVariable = "effectively final variable";
//	    String nonFinalVariable = "non final variable";
//
//	    class InnerClass {         
//	        public void saySomething() {        
//	            System.out.println(finalVariable);
//	            System.out.println(effectivelyFinalVariable);
//	        }   
//	    }   
//
//	    InnerClass instance = new InnerClass();
//	    instance.saySomething();            
//
//	    nonFinalVariable = "new value";
//	}
	

}
