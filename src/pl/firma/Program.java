package pl.firma;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Program {
	
	private static Pracownik[] pracownicy = new Pracownik[6];

	public static void main(String[] args) throws IOException {
		
		test2();
		
//		int i = 0;
//		BufferedReader br = new BufferedReader(new FileReader("C:\\sda\\dane_pracownikow.txt"));
//		try {
//		    String line = br.readLine();
//		    while (line != null) {
//		    	
//		    	if (i > 0) {
//		    		parseLineToPracownik(line, i);
//		    	}
//		    	
//		        line = br.readLine();
//		        i++;
//		    }
//		} finally {
//		    br.close();
//		}

	}

	private static void parseLineToPracownik(String line, int i) {
		
		//List<Pracownik> listaPracownik = new ArrayList<>();
		
		String[] danePracownika = line.split(",");
		PracownikFizyczny pracownikFizyczny = new PracownikFizyczny();
//		
		pracownikFizyczny.numerIdentyfikacyjny = Integer.parseInt(danePracownika[0]);
//		System.out.print(danePracownika[1] + " " + danePracownika[2] + " to: ");
//		
//		if (danePracownika[4].equals("FIZ")) {
//			pracownicy[i-1] = new PracownikFizyczny();
//		} else {
//			pracownicy[i-1] = new PracownikBiurowy();
//		}
		
	}
	
	
	void test() {
		
		OuterClass.InnerClass instance = new OuterClass.InnerClass();
		
		
		Pracownik fileService = new FileService();
		
		FileService realFileService = (FileService) fileService;
		
		//Uzytkowniku, chcesz skorzystac z pliku bazy danych
//		String odpowiedz = null;
//		
//		if (odpowiedz.equals("z pliku")) {
//			service = new FileService();
//		} else {
//			service = new DatabaseService();
//		}
//		
//		System.out.println("Wczytane dane: ");
//		System.out.println(service.getData());
//		
	}
	
	private static void localClassTest() {
		class LocalClass {
			@Override
			public String toString() {
				return "Test";
			}
		}
		LocalClass localClassInstance = new LocalClass();
		System.out.println(localClassInstance);
	}
	
	public void someMethod() {
		final String finalVariable = "final variable";
		String effectivelyFinalVariable = "effectively final variable";
		String nonFinalVariable = "non final variable";
		class InnerClass {
			public void saySomething() {
				System.out.println(finalVariable);
				System.out.println(effectivelyFinalVariable);
			}
		}
		InnerClass instance = new InnerClass();
		instance.saySomething();
		nonFinalVariable = "new value";
	}
	
	
	static void test2() {
		
		Product p = new Product() {
			
			@Override
			public Double getTotalPrice(int count) {
				return null;
			}
			
			@Override
			public String getName() {
				return "Get name test method";
			}
		};
		
		System.out.println(p.getName());
		
	}

}
