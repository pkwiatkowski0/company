package pl.firma;

public interface Product {

	String getName();
	Double getTotalPrice(int count);
	
}
